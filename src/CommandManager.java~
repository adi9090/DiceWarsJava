import java.util.ArrayList;

/**
 * Class is used by ProtocolManager class to execute commands
 */
public class CommandManager {
    private static final String RES_FOLDER_LOCATION = "/home/pc/Projects/AtlantBHZa/res/"; // location of resources folder
    private static final int TOKEN_MAX_GENERATED_NUMBER = 99999;
    private static final int AUCTION_ID_MAX_NUMBER = 99999;

    /**
     * Returns true if user is registered (file with username exist) false if user is not registered.
     * @param username
     * @return true / false
     */
    public static boolean isAlreadyUserRegistered(String username){
        return FilesManipulator.fileExists(RES_FOLDER_LOCATION + "users/" + username);
    }

    /**
     * This function is used to register user.
     * @param username
     * @param password
     */
    public static void registerUser(String username, String password){
        FilesManipulator.createFile(RES_FOLDER_LOCATION + "users/" + username, password);
    }

    /**
     * It checks if passed password for passed username is correct and returns true if it is and false if it is not.
     * @param username
     * @param password
     * @return
     */
    public static boolean isPasswordForUserCorrect(String username, String password){
        String savedPassword = FilesManipulator.getFileContent(RES_FOLDER_LOCATION + "users/" + username, true);
        return savedPassword.equals(password);
    }

    /**
     * It returns true or false if user can be logged in or not.
     * If user is not registered (file with his username does not exist) or if password is wrong it will return false.
     * If password is correct it will return true.
     * @param username
     * @param password
     * @return
     */
    public static boolean loginUser(String username, String password){
        if(!FilesManipulator.fileExists(RES_FOLDER_LOCATION + "users/" + username)){
            return false;
        }
        return isPasswordForUserCorrect(username, password);
    }

    /**
     * It returns token for current user.
     * @param username
     * @return
     */
    public static String generateTokenForUser(String username){
        String token;
        do {
            token = username + "-" + (int)(Math.random() * TOKEN_MAX_GENERATED_NUMBER);
        }while(FilesManipulator.fileExists(RES_FOLDER_LOCATION + "tokens/" + token));
        FilesManipulator.createFile(RES_FOLDER_LOCATION + "tokens/" + token, username);
        return token;
    }

    /**
     * It is used to log out user (it deletes file with user token=
     * @param token user token
     */
    public static void logOutUserWithToken(String token){
        String filePath = RES_FOLDER_LOCATION + "tokens/" + token;
        if(FilesManipulator.fileExists(filePath)){ // delete file
            FilesManipulator.deleteFile(filePath);
        }
    }

    /**
     * Function is used to create auction (create auction file) with passed information about auction
     * @param auctionName
     * @param initialPrice
     * @param minimumPrice
     * @param durationInDays
     * @param auctionCreatedDateAndTime
     * @return
     */
    public static String createAuction(String auctionName, String initialPrice, String minimumPrice,
                                       String durationInDays, String auctionCreatedDateAndTime){
        String auctionId;
        do{
            auctionId = "auct_" + (int)(Math.random() * AUCTION_ID_MAX_NUMBER);
        }while(FilesManipulator.fileExists(RES_FOLDER_LOCATION + "auctions/" + auctionId));
        String auctionInfo = "title=" + auctionName +
                "\ninitialPrice=" + initialPrice +
                "\nminimumPrice=" + minimumPrice +
                "\ndurationInDays=" + durationInDays +
                "\nlastBidValue=!notDefined" +
                "\nlastBidUser=!notDefined" +
                "\ncreatedDate=" + auctionCreatedDateAndTime;
        FilesManipulator.createFile(RES_FOLDER_LOCATION + "auctions/" + auctionId, auctionInfo);
        return "!Ok Auction id: " + auctionId;
    }

    /**
     * Function returns ArrayList of all active auctions.
     * @return
     */
    public static ArrayList<Auction> getActiveAuctions(){
        ArrayList<Auction> auctions = FilesManipulator.getAllAuctions(RES_FOLDER_LOCATION + "auctions");
        ArrayList<Auction> activeAuctions = new ArrayList<>();
        for(int i = 0; i < auctions.size(); i++){
            if(!auctions.get(i).isAuctionExpired()){
                activeAuctions.add(auctions.get(i));
            }
        }
        return activeAuctions;
    }

    /**
     * Function returns ArrayList of all expired auctions.
     * @return
     */
    public static ArrayList<Auction> getExpiredAuctions(){
        ArrayList<Auction> auctions = FilesManipulator.getAllAuctions(RES_FOLDER_LOCATION + "auctions");
        ArrayList<Auction> expiredAuctions = new ArrayList<>();
        for(int i = 0; i < auctions.size(); i++){
            if(auctions.get(i).isAuctionExpired()){
                expiredAuctions.add(auctions.get(i));
            }
        }
        return expiredAuctions;
    }

    /**
     * Function checks if auction exists (if auction file exists) if not it returns error message.
     * It than gets Auction info and updates it if user has bid more than the current bid, if not
     * user gets message that he has not bid high enough.
     * @param bidder
     * @param auctionId
     * @param bidValue
     * @return
     */
    public static String bidOnAuction(String bidder, String auctionId, int bidValue){
        if(!FilesManipulator.fileExists(RES_FOLDER_LOCATION + "auctions/" + auctionId)){
            return "!error Auction: " + auctionId + " does not exist.";
        }
        Auction auction = new Auction(FilesManipulator.getFileContent(RES_FOLDER_LOCATION + "auctions/" + auctionId, false),
                RES_FOLDER_LOCATION + "auctions/" + auctionId);

        System.out.println("isExpired: " + auction.isAuctionExpired());
        if(auction.isAuctionExpired()){
            return "!error Auction has expired.";
        }
        if (auction.getCurrentBid() < bidValue){
            FilesManipulator.createFile(RES_FOLDER_LOCATION + "auctions/" + auctionId, createAuctionData(
                    auction, bidder, bidValue));
            addAuctionToUserList(auctionId, bidder);
            return "!ok";
        }
        return "!error Current bid is higher than Your bid.";
    }

    /**
     * Function is used to create auction info that will be saved to auction file.
     * @param auction
     * @param bidder
     * @param bidValue
     * @return
     */
    private static String createAuctionData(Auction auction, String bidder, int bidValue){
        return "title=" + auction.getTitle() + "\n" +
                "initialPrice=" + auction.getInitialPrice() + "\n" +
                "minimumPrice=" + auction.getMinimumPrice() + "\n" +
                "durationInDays="  + auction.getDurationInDays() + "\n" +
                "lastBidValue=" + bidValue + "\n" +
                "lastBidUser=" + bidder + "\n" +
                "createdDate=" + auction.getCreatedDate();
    }

    /**
     * It adds auction to list of user auction that he / she participated in.
     * @param auctionId
     * @param bidder
     */
    public static void addAuctionToUserList(String auctionId, String bidder) {
        if(!FilesManipulator.fileExists(RES_FOLDER_LOCATION + "userAuctions/" + bidder)){
            FilesManipulator.createFile(RES_FOLDER_LOCATION + "userAuctions/" + bidder, auctionId + ";");
        }else{
            String userAuctionData = FilesManipulator.getFileContent(RES_FOLDER_LOCATION + "userAuctions/" + bidder, true);
            if(!userAuctionData.contains(auctionId)){
                FilesManipulator.createFile(RES_FOLDER_LOCATION + "userAuctions/" + bidder, userAuctionData + auctionId + ";");
            }
        }
    }

    /**
     * Function returns ArrayList of all auctions that user participated in.
     * @param user
     * @return
     */
    public static ArrayList<Auction> getAuctionUserParticipatedIn(String user){
        if(!FilesManipulator.fileExists(RES_FOLDER_LOCATION + "userAuctions/" + user))
            return new ArrayList<>();
        ArrayList<Auction> auctions = new ArrayList<>();
        String participadetAuctions[] = FilesManipulator.getFileContent(RES_FOLDER_LOCATION + "userAuctions/" + user, true).split(";");
        for(int i = 0; i < participadetAuctions.length; i++){
             auctions.add(new Auction(FilesManipulator.getFileContent(RES_FOLDER_LOCATION + "auctions/" +
                    participadetAuctions[i], false), RES_FOLDER_LOCATION + "auctions/" + participadetAuctions[i]));
        }
        return auctions;
    }

    /**
     * Function returns ArrayList of users won auctions.
     * @param user
     * @return
     */
    public static ArrayList<Auction> getWonAuctions(String user) {
        ArrayList<Auction> auctions = FilesManipulator.getAllAuctions(RES_FOLDER_LOCATION + "auctions");
        ArrayList<Auction> wonAuctions = new ArrayList<>();
        for(int i = 0; i < auctions.size(); i++){
            if(auctions.get(i).isAuctionExpired()){
                if(auctions.get(i).getCurrentBidUser().equals(user)){
                    wonAuctions.add(auctions.get(i));
                }
            }
        }
        return wonAuctions;
    }
}
