package GUI;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Inet4Address;
import java.net.UnknownHostException;

/**
 * Created by pc on 6/28/16.
 */
public class CreateGame {
    private static final String[] playersStringArray = { "2 players", "3 players", "4 players", "5 players",
            "6 players", "7 players", "8 players" };
    private JFrame frame;
    private JPanel mainPanel;
    private JPanel northPanel;
    private JLabel serverIP;
    private JComboBox chosePlayer;
    private JTextField playerName;
    private JButton startGameButton;


    public CreateGame() {
        this.frame = new JFrame("Create game");
        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        northPanel = new JPanel();
        northPanel.setLayout(new BorderLayout());
        serverIP = new JLabel("Your IP address is: " + getIP(), SwingConstants.CENTER);
        playerName = new JTextField("Enter Your name");

        chosePlayer = new JComboBox(playersStringArray);
        chosePlayer.addActionListener(new JComboBoxActionListener(chosePlayer));
        northPanel.add(chosePlayer, BorderLayout.CENTER);
        northPanel.add(playerName, BorderLayout.SOUTH);

        startGameButton = new JButton("Start game");
        startGameButton.addActionListener(new StartGameActionListener());

        northPanel.add(serverIP, BorderLayout.NORTH);
        mainPanel.add(startGameButton, BorderLayout.SOUTH);
        mainPanel.add(northPanel, BorderLayout.NORTH);
        frame.add(mainPanel);

        frame.setSize(new Dimension(500, 500));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);

    }


    private String getIP(){
        try {
            String ip = Inet4Address.getLocalHost().getHostAddress();
            return ip;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return "Error getting ip";
    }

    private class JComboBoxActionListener implements ActionListener{
        private JComboBox comboBox;
        public JComboBoxActionListener(JComboBox comboBox) {
            this.comboBox = comboBox;

        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            System.out.println("Selected item: " + comboBox.getSelectedIndex());
        }
    }

    private class StartGameActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            new PlayField(8);
        }
    }


}
