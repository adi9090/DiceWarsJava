package GUI;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class GameRules {
	
	/**
	 * klasa u kojoj se kreira novi prozor koji prikazuje pravila igre
	 */

	public GameRules() {
		super();
		JFrame pravilaIgre = new JFrame("Pravila igre");
		JButton glavniDugmic = new JButton(new ImageIcon("res/ikone_pravila.png"));
		glavniDugmic.setDisabledIcon(new ImageIcon("res/ikone_pravila.png"));
		glavniDugmic.setEnabled(false);
		glavniDugmic.setBackground(Color.white);
		pravilaIgre.add(glavniDugmic);
		pravilaIgre.setResizable(false);
		pravilaIgre.setSize(864,  520);
		pravilaIgre.setLocationRelativeTo(null);
		pravilaIgre.setVisible(true);
		pravilaIgre.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	} // end of public GameRules()
	
}
