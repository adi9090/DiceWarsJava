package GUI.panels;

import GUI.*;
import logic.Field;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

import static utils.Utils.getScaledImage;

/**
 * Created by pc on 4/25/16.
 */
public class CentralPanel extends JPanel {

    private JButton[][] fields;

    public CentralPanel() {
        this.setLayout(new GridLayout(Constants.TABLE_ROWS, Constants.TABLE_COLUMNS));
        this.fields = new JButton[Constants.TABLE_ROWS][Constants.TABLE_COLUMNS];
        for(int i = 0; i < Constants.TABLE_ROWS; i++){
            fields[i] = new JButton[Constants.TABLE_COLUMNS];
            for(int j = 0; j < Constants.TABLE_COLUMNS; j++){
                fields[i][j] = new JButton("");
                fields[i][j].setBorder(Constants.SLIM_BORDER);
                this.add(fields[i][j]);
            }
        }
    }

    public void updateFields(Field [][] table){
        for(int i = 0; i < Constants.TABLE_ROWS; i++){
            for(int j = 0; j < Constants.TABLE_COLUMNS; j++){
                this.fields[i][j].setIcon(getIcon(table[i][j].getNumberOfDices()));
                this.fields[i][j].setBackground(getColor(table[i][j].getPlayer()));
            }
        }
    }

    private Icon getIcon(int numberOfDices) {
        int newImageWidth = 100;
        int newImageHeight = 100;
        return getScaledImage(new ImageIcon("res/field/" + numberOfDices + ".png"),
                newImageWidth, newImageHeight);
    }

    private Color getColor(int player) {
        return Constants.PLAYER_COLORS[player];
    }

}
