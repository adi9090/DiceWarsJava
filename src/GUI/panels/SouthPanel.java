package GUI.panels;

import GUI.Constants;
import utils.Utils;

import javax.swing.*;
import java.awt.*;
import java.nio.ByteOrder;

/**
 * Created by pc on 6/26/16.
 */
public class SouthPanel extends JPanel {
    private JLabel leftSum;
    private JLabel rightSum;
    private JPanel panel;
    private JButton finishMove;
    private JLabel extraDices;
    private JButton emptyButton;

    public SouthPanel() {
        this.setLayout(new BorderLayout());
        this.panel = new JPanel();
        this.panel.setLayout(new GridLayout(1, 5));

        this.leftSum = new JLabel("t", SwingConstants.CENTER);
        this.rightSum = new JLabel("f", SwingConstants.CENTER);
        this.finishMove = new JButton();
        this.finishMove.setIcon(Utils.getScaledImage(new ImageIcon("res/endMove/shutdown_1.png"), 75, 75));
        this.finishMove.setRolloverIcon(Utils.getScaledImage(new ImageIcon("res/endMove/shutdown_0.png"), 75, 75));
        this.extraDices = new JLabel("0", SwingConstants.CENTER);
        this.emptyButton = new JButton();
        this.emptyButton.setEnabled(false);

        this.panel.add(new JLabel());
        this.panel.add(this.extraDices);
        this.panel.add(this.finishMove);
        this.panel.add(new JLabel());
        this.panel.add(new JLabel());

        this.add(this.leftSum, BorderLayout.WEST);
        this.add(this.rightSum, BorderLayout.EAST);
        this.add(this.panel, BorderLayout.CENTER);
    }

    public void updateFields(int player){
        this.finishMove.setBackground(Constants.PLAYER_COLORS[player]);
    }
}
