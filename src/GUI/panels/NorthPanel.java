package GUI.panels;

import GUI.Constants;

import javax.swing.*;
import java.awt.*;

/**
 * Created by pc on 6/26/16.
 */
public class NorthPanel extends JPanel{
    private int numberOfPlayers;
    private JLabel[] fields;

    public NorthPanel(int numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
        this.setLayout(new GridLayout(1, this.numberOfPlayers));
        this.fields = new JLabel[this.numberOfPlayers];
        for(int i = 0; i < this.numberOfPlayers; i++) {
            this.fields[i] = new JLabel("TEST", SwingConstants.CENTER);
            this.fields[i].setOpaque(true);
            this.fields[i].setBackground(Constants.PLAYER_COLORS[i]);
            this.add(this.fields[i]);
        }
    }

    public void updatePanel(int connectedFields[]){
        for(int i = 0; i < numberOfPlayers; i++){
            this.fields[i].setText("" + connectedFields[i]);

        }
    }


}
