package GUI.panels;

import GUI.Constants;
import utils.Utils;

import javax.swing.*;
import java.awt.*;

/**
 * Created by pc on 6/26/16.
 */
public class SidePanel extends JPanel {
    private JButton[] dices;

    public SidePanel() {
        this.setLayout(new GridLayout(8, 1));
        this.dices = new JButton[8];
        for(int i = 0; i < 8; i++){
            this.dices[i] = new JButton();
            this.dices[i].setEnabled(false);
            this.dices[i].setIcon(Utils.getScaledImage(new ImageIcon("res/thrown/transparent.png"), 50, 50));
            this.dices[i].setDisabledIcon(Utils.getScaledImage(new ImageIcon("res/thrown/transparent.png"), 50, 50));
            this.dices[i].setBorder(Constants.NO_BORDER);
            this.add(dices[i]);
        }
    }

    public void updatePanel(int dices[], int player){
        for(int i = 0; i < 8; i++){
            if(dices[i] != 0){
                this.dices[i].setDisabledIcon(Utils.getScaledImage(new ImageIcon("res/thrown/" + dices[i] + ".png"), 50, 50));
            }else{
                this.dices[i].setDisabledIcon(Utils.getScaledImage(new ImageIcon("res/thrown/transparent.png"), 50, 50));
            }
            this.dices[i].setBackground(Constants.PLAYER_COLORS[player]);
        }
    }


}
