package GUI;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class PlayFieldInfo {
	/**
	 * klasa koja kreira novi prozor u kojem je prikazan izgled poligona za igru
	 */
	public PlayFieldInfo() {
		super();
		
		JFrame pravilaIgre = new JFrame("Izgled poligona");
		JButton glavniDugmic = new JButton(new ImageIcon("res/ikone_tabela.png"));
		glavniDugmic.setDisabledIcon(new ImageIcon("res/ikone_tabela.png"));
		glavniDugmic.setEnabled(false);
		glavniDugmic.setBackground(Color.white);
		pravilaIgre.add(glavniDugmic);
		pravilaIgre.setResizable(false);
		pravilaIgre.setSize(860, 477);
		pravilaIgre.setLocationRelativeTo(null);
		pravilaIgre.setVisible(true);
		pravilaIgre.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	} // end of public PlayFieldInfo()

}
