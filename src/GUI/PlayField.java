package GUI;

import GUI.panels.CentralPanel;
import GUI.panels.NorthPanel;
import GUI.panels.SidePanel;
import GUI.panels.SouthPanel;
import logic.GameLogic;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by pc on 4/25/16.
 */
public class PlayField {

    private JFrame frame;
    private JPanel mainPanel;
    private CentralPanel centralPanel;
    private NorthPanel northPanel;
    private SidePanel eastPanel;
    private SidePanel westPanel;
    private SouthPanel southPanel;
    private static final Dimension FRAME_DIMENSION = new Dimension(1280, 720);
    private static final int CENTRAL_PANEL_ROWS = 7;
    private static final int CENTRAL_PANEL_COLUMNS = 12;
    private static final int NUMBER_OF_PLAYERS = 2;
    private int numberOfPlayers;
    private GameLogic game;

    public PlayField(int players) {
        this.frame = new JFrame("Dice wars");
        this.numberOfPlayers = players;
        this.game = new GameLogic(this.numberOfPlayers);

        this.mainPanel = new JPanel();
        this.centralPanel = new CentralPanel();
        this.southPanel = new SouthPanel();
        this.northPanel = new NorthPanel(this.game.getNumberOfPlayers());
        this.eastPanel = new SidePanel();
        this.westPanel = new SidePanel();

        mainPanel.setLayout(new BorderLayout());

        mainPanel.add(centralPanel, BorderLayout.CENTER);
        mainPanel.add(eastPanel, BorderLayout.EAST);
        mainPanel.add(westPanel, BorderLayout.WEST);
        mainPanel.add(northPanel, BorderLayout.NORTH);
        mainPanel.add(southPanel, BorderLayout.SOUTH);
        frame.add(mainPanel);

        centralPanel.updateFields(game.getFields());
        northPanel.updatePanel(game.getNumberOfConnectedFields());
        eastPanel.updatePanel(new int[]{0, 1, 2, 3, 4, 5, 6, 0}, 0);
        westPanel.updatePanel(new int[]{0, 1, 2, 3, 4, 5, 6, 0}, 1);
        southPanel.updateFields(0);

        this.frame.setSize(FRAME_DIMENSION);
        this.frame.setMinimumSize(FRAME_DIMENSION);
        this.frame.setMaximumSize(FRAME_DIMENSION);
        this.frame.setResizable(false);
//        this.frame.setLocationRelativeTo(null);
        this.frame.setVisible(true);
        this.frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        this.frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                new InitialFrame();
            }
        });

    }



}
