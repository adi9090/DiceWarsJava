package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by pc on 6/28/16.
 */
public class JoinGame {
    private JFrame frame;
    private JPanel mainPanel;
    private JPanel centralPanel;
    private JTextField playerName;
    private JTextField serverIP;
    private static final Dimension FRAME_DIMENSION = new Dimension(500, 100);

    private JButton join;

    public JoinGame() {
        frame = new JFrame("Join game");

        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        centralPanel = new JPanel();
        centralPanel.setLayout(new GridLayout(2, 2));

        playerName = new JTextField("Hasim");
        serverIP = new JTextField("192.105.244.11");
        centralPanel.add(new JLabel("Enter Your name: ", SwingConstants.CENTER));
        centralPanel.add(playerName);
        centralPanel.add(new JLabel("Enter server ip address: ", SwingConstants.CENTER));
        centralPanel.add(serverIP);

        join = new JButton("Join game");
        join.addActionListener(new JoinActionListener());
        mainPanel.add(join, BorderLayout.SOUTH);


        mainPanel.add(centralPanel, BorderLayout.CENTER);

        frame.add(mainPanel);

        frame.setSize(FRAME_DIMENSION);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

    public class JoinActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (playerName.getText().length() < 3) {
                JOptionPane.showMessageDialog(null, "Player name must contain minimum of 4 letters");
            } else {

            }
        }
    }

}

