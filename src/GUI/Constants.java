package GUI;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by pc on 5/1/16.
 */
public class Constants {
    public static final Color[] PLAYER_COLORS = {new Color(71, 135,255),
            new Color(121, 255, 117),
            new Color(255, 51, 44),
            new Color(255, 239, 82),
            new Color(101, 255, 21),
            new Color(255, 87, 214),
            new Color(84, 231, 216),
            new Color(255, 118, 71)};
    public static final Color SELECTED_FIELD_COLOR = new Color(128, 128, 128);
    public static final int TABLE_ROWS = 8;
    public static final int TABLE_COLUMNS = 10;
    public static final int MAX_NUMBER_OF_PLAYERS = 8;
    public static final MatteBorder NO_BORDER = BorderFactory.createMatteBorder(0, 0, 0, 0, Color.black);
    public static final MatteBorder SLIM_BORDER = BorderFactory.createMatteBorder(1, 1, 0, 0, Color.black);

}
