package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by pc on 4/24/16.
 */
public class InitialFrame {

    private JFrame mainWindow;
    private JPanel mainPanel;
    private JMenuBar menuBar;
    private JButton background;
    private JButton gameLogo;
    private JMenu file;
    private JMenu help;
    private JMenu about;
    private JMenu newGame;
    private JMenuItem createGame;
    private JMenuItem joinGame;
    private JMenuItem exit;
    private JMenuItem gameRules;
    private JMenuItem playFieldHelp;
    private JMenuItem info;

    public InitialFrame() {
        mainWindow = new JFrame("Rat kockica");

        menuBar = new JMenuBar();

        file = new JMenu("File");
        help = new JMenu("Help");
        about = new JMenu("About");

        newGame = new JMenu("Nova igra");
        exit = new JMenuItem("Exit");
        gameRules = new JMenuItem("Pravila igre");
        playFieldHelp = new JMenuItem("Izgled poligona");
        info = new JMenuItem("Info");

        createGame = new JMenuItem("Create game");
        joinGame = new JMenuItem("Join game");
        createGame.addActionListener(new JMenuActionListener(createGame));
        joinGame.addActionListener(new JMenuActionListener(joinGame));
        newGame.add(createGame);
        newGame.add(joinGame);


        playFieldHelp.addActionListener(new JMenuActionListener(playFieldHelp));
        gameRules.addActionListener(new JMenuActionListener(gameRules));
        info.addActionListener(new JMenuActionListener(info));
        exit.addActionListener(new JMenuActionListener(exit));

        file.add(newGame);
        file.add(exit);
        help.add(gameRules);
        help.add(playFieldHelp);
        about.add(info);

        menuBar.add(file);
        menuBar.add(help);
        menuBar.add(about);

        mainWindow.setJMenuBar(menuBar);

        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        background = new JButton(new ImageIcon("res/ikone_pozadina.png"));
        background.setDisabledIcon(new ImageIcon("res/ikone_pozadina.png"));
        background.setBackground(Color.black);
        background.setEnabled(false);
        background.setSize(794, 459);

        gameLogo = new JButton(new ImageIcon("res/ikone_logoIgre.png"));
        gameLogo.setDisabledIcon(new ImageIcon("res/ikone_logoIgre.png"));
        gameLogo.setBackground(Color.white);
        gameLogo.setSize(794, 95);
        gameLogo.setEnabled(false);
        mainPanel.add(gameLogo, BorderLayout.NORTH);
        mainPanel.add(background, BorderLayout.CENTER);

        mainWindow.setSize(794, 590);
        mainWindow.add(mainPanel);
        mainWindow.setResizable(false);
        mainWindow.setLocationRelativeTo(null);
        mainWindow.setVisible(true);
        mainWindow.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private class JMenuActionListener implements ActionListener {

        private JMenuItem menuItem;

        public JMenuActionListener(JMenuItem menuItem) {
            this.menuItem = menuItem;
        }

        @Override
        public void actionPerformed(ActionEvent e) {

            if(menuItem.getText().equals("Create game")){
                new CreateGame();
                mainWindow.dispose();
            }

            if(menuItem.getText().equals("Join game")){
                new JoinGame();
                mainWindow.dispose();
            }

            if (menuItem.getText().equals("Pravila igre")) {
                new GameRules();
            }

            if (menuItem.getText().equals("Exit")) {
                mainWindow.dispose();
            }

            if (menuItem.getText().equals("Izgled poligona")) {
                new PlayFieldInfo();
            }

            if (menuItem.getText().equals("Info")) {
                new About();
            }

        }
    }
}
