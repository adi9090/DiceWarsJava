package GUI;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class About {
	/**
	 * moram da uzmem zasluge za uradjeno :D
	 */
	public About() {
		super();
		JFrame frame = new JFrame("Info");
		JButton slika = new JButton(new ImageIcon("res/ikone_info.png"));
		slika.setDisabledIcon(new ImageIcon("res/ikone_info.png"));
		slika.setEnabled(false);
		frame.add(slika);
		frame.setSize(920, 570);
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	} // end of public About()

}
