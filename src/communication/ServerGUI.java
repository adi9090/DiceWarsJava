package communication;

/**
 * Created by pc on 7/1/16.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/*
 * The server as a GUI
 */
public class ServerGUI extends JFrame implements ActionListener, WindowListener {

    private static final long serialVersionUID = 1L;
    // the stop and start buttons
    private JButton stopStartJButton;
    // JTextArea for the chatTextArea room and the events
    private JTextArea chatTextArea, eventTextArea;
    // The port number
    private JTextField tPortNumberTextField;
    // my server
    private Server server;


    // server constructor that receive the port to listen to for connection as parameter
    ServerGUI(int port) {
        super("Chat Server");
        server = null;
        // in the NorthPanel the PortNumber the Start and Stop buttons
        JPanel north = new JPanel();
        north.add(new JLabel("Port number: "));
        tPortNumberTextField = new JTextField("  " + port);
        north.add(tPortNumberTextField);
        // to stop or start the server, we start with "Start"
        stopStartJButton = new JButton("Start");
        stopStartJButton.addActionListener(this);
        north.add(stopStartJButton);
        add(north, BorderLayout.NORTH);

        // the eventTextArea and chatTextArea room
        JPanel center = new JPanel(new GridLayout(2,1));
        chatTextArea = new JTextArea(80,80);
        chatTextArea.setEditable(false);
        appendRoom("Chat room.\n");
        center.add(new JScrollPane(chatTextArea));
        eventTextArea = new JTextArea(80,80);
        eventTextArea.setEditable(false);
        appendEvent("Events log.\n");
        center.add(new JScrollPane(eventTextArea));
        add(center);

        // need to be informed when the user click the close button on the frame
        addWindowListener(this);
        setSize(400, 600);
        setVisible(true);
    }

    // append message to the two JTextArea
    // position at the end
    void appendRoom(String str) {
        chatTextArea.append(str);
        chatTextArea.setCaretPosition(chatTextArea.getText().length() - 1);
    }
    void appendEvent(String str) {
        eventTextArea.append(str);
        eventTextArea.setCaretPosition(chatTextArea.getText().length() - 1);

    }

    // start or stop where clicked
    public void actionPerformed(ActionEvent e) {
        // if running we have to stop
        if(server != null) {
            server.stop();
            server = null;
            tPortNumberTextField.setEditable(true);
            stopStartJButton.setText("Start");
            return;
        }
        // OK start the server
        int port;
        try {
            port = Integer.parseInt(tPortNumberTextField.getText().trim());
        }
        catch(Exception er) {
            appendEvent("Invalid port number");
            return;
        }
        // ceate a new Server
        server = new Server(port, this);
        // and start it as a thread
        new ServerRunning().start();
        stopStartJButton.setText("Stop");
        tPortNumberTextField.setEditable(false);
    }

    // entry point to start the Server
    public static void main(String[] arg) {
        // start server default port 1500
        new ServerGUI(1500);
    }

    /*
     * If the user click the X button to close the application
     * I need to close the connection with the server to free the port
     */
    public void windowClosing(WindowEvent e) {
        // if my Server exist
        if(server != null) {
            try {
                server.stop();			// ask the server to close the conection
            }
            catch(Exception eClose) {
            }
            server = null;
        }
        // dispose the frame
        dispose();
        System.exit(0);
    }
    // I can ignore the other WindowListener method
    public void windowClosed(WindowEvent e) {}
    public void windowOpened(WindowEvent e) {}
    public void windowIconified(WindowEvent e) {}
    public void windowDeiconified(WindowEvent e) {}
    public void windowActivated(WindowEvent e) {}
    public void windowDeactivated(WindowEvent e) {}

    /*
     * A thread to run the Server
     */
    class ServerRunning extends Thread {
        public void run() {
            server.start();         // should execute until if fails
            // the server failed
            stopStartJButton.setText("Start");
            tPortNumberTextField.setEditable(true);
            appendEvent("Server crashed\n");
            server = null;
        }
    }

}


