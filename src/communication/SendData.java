package communication;

import logic.Field;

import java.io.Serializable;

/**
 * Created by pc on 7/1/16.
 */
public class SendData implements Serializable{

    public static final int WHOISIN = 0, MESSAGE = 1, LOGOUT = 2;
    private int type;
    private String message;
    private Field[][] fields;
    private int[] connectedFields;
    private int[] thrownDicesFromPlayerField;
    private int[] thrownDicesFromAttackedField;
    private Field activePlayerSelectedField;
    private Field attackedField;

    public SendData(int type, String message) {
        this.type = type;
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Field[][] getFields() {
        return fields;
    }

    public void setFields(Field[][] fields) {
        this.fields = fields;
    }

    public int[] getConnectedFields() {
        return connectedFields;
    }

    public void setConnectedFields(int[] connectedFields) {
        this.connectedFields = connectedFields;
    }

    public int[] getThrownDicesFromPlayerField() {
        return thrownDicesFromPlayerField;
    }

    public void setThrownDicesFromPlayerField(int[] thrownDicesFromPlayerField) {
        this.thrownDicesFromPlayerField = thrownDicesFromPlayerField;
    }

    public int[] getThrownDicesFromAttackedField() {
        return thrownDicesFromAttackedField;
    }

    public void setThrownDicesFromAttackedField(int[] thrownDicesFromAttackedField) {
        this.thrownDicesFromAttackedField = thrownDicesFromAttackedField;
    }

    public Field getActivePlayerSelectedField() {
        return activePlayerSelectedField;
    }

    public void setActivePlayerSelectedField(Field activePlayerSelectedField) {
        this.activePlayerSelectedField = activePlayerSelectedField;
    }

    public Field getAttackedField() {
        return attackedField;
    }

    public void setAttackedField(Field attackedField) {
        this.attackedField = attackedField;
    }
}
