package logic;

/**
 * Created by pc on 5/1/16.
 */
public class Field {
    private int player;
    private int numberOfDices;
    private int x;
    private int y;

    public Field() {
    }

    public Field(int player, int numberOfDices, int x, int y) {
        this.player = player;
        this.numberOfDices = numberOfDices;
        this.x = x;
        this.y = y;
    }

    public int getPlayer() {
        return player;
    }

    public void setPlayer(int player) {
        this.player = player;
    }

    public int getNumberOfDices() {
        return numberOfDices;
    }

    public void setNumberOfDices(int numberOfDices) {
        this.numberOfDices = numberOfDices;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
