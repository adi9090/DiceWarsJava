package logic;

import GUI.Constants;

/**
 * Created by pc on 5/1/16.
 */
public class GameLogic {
    private int numberOfPlayers;
    private int totalFields;
    private Field[][] fields;

    public GameLogic(int numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
        this.totalFields = Constants.TABLE_ROWS * Constants.TABLE_COLUMNS;

        fields = new Field[Constants.TABLE_ROWS][Constants.TABLE_COLUMNS];
        for(int i = 0; i < Constants.TABLE_ROWS; i++){
            fields[i] = new Field[Constants.TABLE_COLUMNS];
            for(int j = 0; j < Constants.TABLE_COLUMNS; j++){
                int player = (int)(Math.random() * this.numberOfPlayers);
                int dices = 1 + (int)(Math.random() * 7);
                fields[i][j] = new Field(player, dices, i, j);
            }
        }
    }

    private void createTable(){
        for(int i = 0; i < Constants.TABLE_ROWS; i++){
            for(int j = 0; j < Constants.TABLE_COLUMNS; j++){

                fields[i][j].setPlayer((int)(Math.random() * this.numberOfPlayers));
                fields[i][j].setNumberOfDices(1 + (int)(Math.random() * 7));
            }
        }
    }

    public Field[][] getFields(){
        return fields;
    }

    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public void setNumberOfPlayers(int numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
    }

    public int getTotalFields() {
        return totalFields;
    }

    public void setTotalFields(int totalFields) {
        this.totalFields = totalFields;
    }

    public void setFields(Field[][] fields) {
        this.fields = fields;
    }

    public int[] getNumberOfConnectedFields() {
        int connectedFields[] = new int[this.numberOfPlayers];
        for(int i = 0; i < this.numberOfPlayers; i++){
            connectedFields[i] = (int)(Math.random() * 20);
        }
        return connectedFields;
    }
}
